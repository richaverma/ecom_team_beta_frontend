import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http:HttpClient) { }
  get(url:any)
  {
    return this.http.get(url)
  }

  public post(url: string, data?: any, options?: any) {
    return this.http.post(url, data, options);
  }  
}

