import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServicesService } from 'src/services/services.service';


@Component({
  selector: 'app-addcategory',
  templateUrl: './addcategory.component.html',
  styleUrls: ['./addcategory.component.scss']
})
export class AddcategoryComponent implements OnInit {

  constructor(private api:ServicesService,private formbuilder:FormBuilder,) { }
  categories:any;
  apiurl:any;
  ngOnInit(): void {
this.getAllCategory()
  }
  CategoryForm: FormGroup = this.formbuilder.group({
    catName: ["", [Validators.required]],
  })

  getAllCategory()
  {
    let apiurl = `https://localhost:44390/api/ApiGateway/get-all-category`;
    this.api.get(apiurl).subscribe(res=>{
      this.categories=res;
      console.log(this.categories)
    })
  }

  trackByIndex = (index:number):number =>{
    return index;
  }

  NewCategory(){
    {
      if(this.CategoryForm.status=='VALID')
      {

        // let url1 = apiurl+"/add-new-vategory";
        let url = `https://localhost:44390/api/ApiGateway/add-new-category`;
        let body ={
          
          categoryName: this.CategoryForm.value.catName,

        }
        this.api.post(url,body).subscribe(res=>{
          console.log(res);
          this.categories=res;
          alert("Added New Category!")
        })
        
      }else{
        alert("Error While Adding Product!")
       console.log("Not Working!!")
      }
    }
  }
}
