import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/services/data.service';
import { ServicesService } from 'src/services/services.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formbuilder: FormBuilder, private api: ServicesService, private dataservice: DataService, private route: Router) { }

  ngOnInit(): void {
  }

  LoginForm: FormGroup = this.formbuilder.group({
    email: ["", [Validators.required]],
    password: ["", [Validators.required]],

  });
  NewLogin() {
    if (this.LoginForm.status == 'VALID') {
      var email = this.LoginForm.value.email;
      console.log(email)
      let url = "https://localhost:44390/api/ApiGateway/get-logindetail-email/" + email;
      this.api.post(url).subscribe(response => {
        if (response == null) {
          alert("User not Exists!!")
        }
        else {
          let jsonResponse = JSON.parse(JSON.stringify(response));
          if (jsonResponse.password == this.LoginForm.value.password) {
            console.log("Logged On!!");
            console.log(jsonResponse);
            let loginId = jsonResponse.loginId;
            console.log(loginId)
            let url = `https://localhost:44390/api/ApiGateway/get-customer-by-id/${loginId}`;
            console.log(url);
            this.api.post(url).subscribe(r => {
              let custRes = JSON.parse(JSON.stringify(r));
              console.log("cust", custRes);
              let body = {
                loginId: jsonResponse.loginId,
                loginRole: jsonResponse.loginRole,
                token: jsonResponse.token,
                userId: jsonResponse.userId,
                customerId: custRes.customerId,
                address: custRes.customerAddress
              }
              localStorage.setItem('User', JSON.stringify(body));
              this.dataservice.setUser(body)
              this.route.navigateByUrl('app-viewproduts');
              
            })
          } else {
            alert("Invalid Credentials!")
          }

        }
      })
    }
  }

}
