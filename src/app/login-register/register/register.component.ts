import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServicesService } from 'src/services/services.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(private formbuilder: FormBuilder, private api: ServicesService,private router:Router) { }
  RegisterForm: FormGroup = this.formbuilder.group({
    name: ['', [Validators.required]],

    address: ['', [Validators.required]],

    email: ['', [Validators.required]],

    phoneno: ['', [Validators.required]],

    password: ['', [Validators.required]],

    confirmpassword: ['', [Validators.required]],
  });
  ngOnInit(): void { }
  AddCustomer() {
    if (this.RegisterForm.status == 'VALID') {
      if (this.RegisterForm.value.password == this.RegisterForm.value.confirmpassword) {
        let loginUrl = `https://localhost:44390/api/ApiGateway/get-logindetail-email/${this.RegisterForm.value.email}`;
        // this.api.post(loginUrl).subscribe(res=>{
        //   console.log(res);
        // })
        var email = this.RegisterForm.value.email
        let login = {
          userEmail: this.RegisterForm.value.email,
          password: this.RegisterForm.value.password,
          loginRole: "User"
        }
        this.api.post(`https://localhost:44390/api/ApiGateway/new-login`, login).subscribe(response => {
          var res = JSON.parse(JSON.stringify(response))
          var login_Id = res.loginId;
          console.log(login_Id)
          let customer = {
            loginId: login_Id,
            customerName: this.RegisterForm.value.name,
            customerAddress: this.RegisterForm.value.address,
            customerPhoneNumber: this.RegisterForm.value.phoneno,
            customerEmailId: this.RegisterForm.value.email,
          }
          this.api.post(`https://localhost:44390/api/ApiGateway/add-new-customer`, customer).subscribe(response => {
            console.log(response);
            var res = JSON.parse(JSON.stringify(response))
            this.router.navigateByUrl('/app-login');
          })
        })
      }
      else {
        alert("Passwords don't match")
      }

    }
    else {
      alert("Please enter valid credentials")
    }
  }

}
