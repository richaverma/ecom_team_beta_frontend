import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewprodutsComponent } from './viewproduts.component';

describe('ViewprodutsComponent', () => {
  let component: ViewprodutsComponent;
  let fixture: ComponentFixture<ViewprodutsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewprodutsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewprodutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
