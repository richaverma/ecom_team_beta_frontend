import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServicesService } from 'src/services/services.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss']
})
export class AddproductComponent implements OnInit {

  constructor(private formbuilder: FormBuilder, private api: ServicesService) { }
  ngOnInit(): void {
  }
  AddProduct: FormGroup = this.formbuilder.group({
    categoryId:['', [Validators.required]],
    productName:['', [Validators.required]],
    productType:['', [Validators.required]],
    productPrice:['', [Validators.required]],
    productDes:['', [Validators.required]],
    productLink:['', [Validators.required]]
  })

  NewProduct()
  {
    if(this.AddProduct.status=='VALID')
    {
      let url = `https://localhost:44390/api/ApiGateway/add-new-product`;
      let body ={
        
        categoryId: parseInt(this.AddProduct.value.categoryId),
        productName: this.AddProduct.value.productName,
        productType: this.AddProduct.value.productType,
        productPrice: this.AddProduct.value.productPrice,
        productDescription: this.AddProduct.value.productDes,
        productImage: this.AddProduct.value.productLink
      }
      this.api.post(url,body).subscribe(res=>{
        console.log(res);
        alert("Added New Product!")
      })
      
    }else{
      alert("Error While Adding Product!")
     console.log("Not Working!!")
    }
  }
}
