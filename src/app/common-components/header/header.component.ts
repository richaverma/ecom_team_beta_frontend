import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router) { }
  login=false;
  role:any;
  ngOnInit(): void {
    let data = JSON.parse(localStorage.getItem("User")||"{}")
    this.role = data.loginRole;
    if(this.role)
    {
      this.login=true;
    }
    console.log("Role", this.role);
  }


  logout()
  {
    localStorage.clear();
    this.router.navigateByUrl("app-login")
    location.reload();
  }

}
